package com.elevator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Elevator {
    private static final int CAPACITY = 5;
    private static final Direction DEFAULT_DIRECTION = Direction.DOWN;

    private final Integer maxFloorNumber;

    private List<Passenger> passengers = new ArrayList<>();

    private Direction direction;

    private Integer currentFloorNumber;

    public Elevator(Integer maxFloorNumber) {
        this.maxFloorNumber = maxFloorNumber;
        this.direction = Direction.NOT_SET;
    }

    public Elevator chooseDirection(List<Passenger> currentFloorPassengers) {
        if (!this.passengers.isEmpty()) {
            return this;
        }
        if (currentFloorPassengers == null || currentFloorPassengers.isEmpty()) {
            if (direction == null || direction.equals(Direction.DOWN) || direction.equals(Direction.NOT_SET)) {
                if (currentFloorNumber != 1) {
                    this.direction = Direction.DOWN;
                } else this.direction = Direction.UP;
            } else if (direction.equals(Direction.UP)) {
                if (!currentFloorNumber.equals(maxFloorNumber)) {
                    this.direction = Direction.UP;
                } else this.direction = Direction.DOWN;
            }
            return this;
        }
        long passengersWhoGoUp = currentFloorPassengers.stream()
                .filter(s -> s.getDestinationFloor() > currentFloorNumber)
                .count();
        long passengersWhoGoDown = currentFloorPassengers.stream()
                .filter(s -> s.getDestinationFloor() < currentFloorNumber)
                .count();

        if (passengersWhoGoDown == passengersWhoGoUp) {
            this.direction = DEFAULT_DIRECTION;
            return this;
        }
        this.direction = passengersWhoGoDown > passengersWhoGoUp ? Direction.DOWN : Direction.UP;
        return this;
    }

    public List<Passenger> pollPassengersByFloorNumber(Integer floorNumber) {
        if (passengers == null) {
            return Collections.emptyList();
        }
        List<Passenger> polledPassengers = passengers.stream()
                .filter(passenger -> passenger.getDestinationFloor().equals(floorNumber))
                .collect(Collectors.toList());
        passengers.removeAll(polledPassengers);
        return polledPassengers;
    }

    public List<Passenger> putPassengers(List<Passenger> passengers) {
        if (passengers == null || passengers.isEmpty()) {
            return Collections.emptyList();
        }
        if (this.passengers.size() <= CAPACITY) {
            int freePlaces = CAPACITY - this.passengers.size();
            List<Passenger> selectedPassengers;
            if (freePlaces - passengers.size() >= 0) {
                selectedPassengers = passengers.subList(0, passengers.size());
            } else {
                selectedPassengers = passengers.subList(0, freePlaces);
            }
            this.passengers.addAll(selectedPassengers);
            return selectedPassengers;
        }
        return Collections.emptyList();
    }

    public Elevator setCurrentFloorNumber(Integer currentFloorNumber) {
        this.currentFloorNumber = currentFloorNumber;
        if (passengers != null) {
            passengers.forEach(passenger -> passenger.setCurrentFloorNumber(currentFloorNumber));
        }
        return this;
    }

    public Direction getDirection() {
        return direction;
    }


    public Integer getMaxFloorNumber() {
        return maxFloorNumber;
    }

    public enum Direction {
        UP, DOWN, NOT_SET;
    }

    @Override
    public String toString() {
        String passengersInElevator = IntStream.range(0, CAPACITY)
                .boxed()
                .map(place ->
                        Optional.ofNullable(passengers)
                                .map(s -> place < s.size() ? s.get(place) : null)
                                .map(Passenger::toString)
                                .orElse(" ")
                )
                .collect(Collectors.joining(" "));
        switch (direction) {
            case UP:
                return "^ " +
                        passengersInElevator +
                        "^";
            case DOWN:
                return "v " +
                        passengersInElevator +
                        " v";
            case NOT_SET:
                return "  " +
                        passengersInElevator +
                        "  ";
            default:
                return null;
        }
    }
}
