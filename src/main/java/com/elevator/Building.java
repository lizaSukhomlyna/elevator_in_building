package com.elevator;

import java.util.*;
import java.util.stream.Collectors;

public class Building {
    private Map<Integer, Floor> floorNumberToFloor;

    public Building(List<Floor> floors, Elevator elevator) {
        this.floorNumberToFloor = floors.stream().collect(Collectors.toMap(Floor::getFloorNumber, s -> s));
        this.floorNumberToFloor.get(1).setElevator(
                elevator.setCurrentFloorNumber(
                        floorNumberToFloor.get(1).getFloorNumber()
                )
        );
    }

    public void startWork() {
        while (true) {
            try {
                System.out.println("*************");
                System.out.println(this);
                Thread.sleep(5000);//пауза между кадрами
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            //поиск этажа с лифтом
            Floor floorWithElevator = floorNumberToFloor.values().stream()
                    .filter(floor -> floor.getElevator() != null)
                    .findFirst().orElse(null);
            if (floorWithElevator == null) {
                throw new UnsupportedOperationException("No Floor with elevator");
            }

            floorWithElevator.disembarkPassengersFromElevator();
            floorWithElevator.getElevator().chooseDirection(floorWithElevator.getPassengers());
            floorWithElevator.loadElevatorWithPassengers();

            moveElevator(floorWithElevator);

        }

    }

    private void moveElevator(Floor floorWithElevator) {
        Integer newFloorNumberForElevator = Optional.of(floorWithElevator)
                .map(Floor::getElevator)
                .map(Elevator::getDirection)
                .filter(s -> s.equals(Elevator.Direction.DOWN) || s.equals(Elevator.Direction.UP))
                .map(s -> s.equals(Elevator.Direction.DOWN) ?
                        floorWithElevator.getFloorNumber() - 1 :
                        floorWithElevator.getFloorNumber() + 1
                )
                .orElseThrow(() -> new UnsupportedOperationException("Error while moving elevator, destination is not set"));
        floorNumberToFloor
                .get(newFloorNumberForElevator)
                .setElevator(floorWithElevator.getElevator()
                        .setCurrentFloorNumber(newFloorNumberForElevator)
                );
        floorWithElevator.setElevator(null);
    }

    @Override
    public String toString() {
        //сортировка, для порядка этажей, от верхнего к нижнему
        return floorNumberToFloor.values().stream()
                .sorted(Comparator.comparing(Floor::getFloorNumber).reversed())
                .map(Floor::toString)
                .collect(Collectors.joining("\n"));
    }
}
