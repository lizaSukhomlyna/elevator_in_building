package com.elevator;

import java.util.List;
import java.util.stream.Collectors;

import static com.elevator.Elevator.Direction.DOWN;
import static com.elevator.Elevator.Direction.UP;

public class Floor {
    private List<Passenger> passengers;

    private final Integer floorNumber;

    private Elevator elevator;

    public Floor(Integer floorNumber, List<Passenger> passengers, Elevator elevator) {
        this.floorNumber = floorNumber;
        this.passengers = passengers;
        this.elevator = elevator;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setElevator(Elevator elevator) {
        this.elevator = elevator;
    }

    public Elevator getElevator() {
        return elevator;
    }

    public void disembarkPassengersFromElevator() {
        if (elevator == null) {
            throw new UnsupportedOperationException("Elevator doesn't exist on this floor: " + this);
        }
        List<Passenger> passengersFromElevator = elevator.pollPassengersByFloorNumber(floorNumber);
        passengersFromElevator.forEach(pas -> pas.setDestinationFloor(pas.chooseNextRandomFloorNumber(elevator.getMaxFloorNumber(), floorNumber)));
        passengers.addAll(passengersFromElevator);
    }

    //в зависиммости, куда идёт лифт, загружаем ему подходящих пассажиров с этого этажа
    public void loadElevatorWithPassengers() {
        List<Passenger> passengersToLoad = passengers.stream()
                .filter(passenger -> (elevator.getDirection().equals(DOWN) && passenger.getDestinationFloor() < floorNumber) ||
                        (elevator.getDirection().equals(UP) && passenger.getDestinationFloor() > floorNumber)
                ).collect(Collectors.toList());
        List<Passenger> puttedPassengers = elevator.putPassengers(passengersToLoad);
        passengers.removeAll(puttedPassengers);
    }

    @Override
    public String toString() {
        String elevatorRepresentation = elevator == null ? "|           |" : elevator.toString();
        String floorNumberRepresentation = floorNumber < 10 ? floorNumber + " " : floorNumber.toString();
        return floorNumberRepresentation + " |" + elevatorRepresentation + "| " +
                passengers.stream()
                        .map(Passenger::toString)
                        .collect(Collectors.joining(" "));
    }
}
