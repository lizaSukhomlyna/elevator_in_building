package com.elevator;

import java.util.Objects;

public class Passenger {
    private Integer destinationFloor;

    private Integer currentFloorNumber;

    public Passenger(Integer maxFloor, Integer currentFloorNumber) {
        this.destinationFloor = chooseNextRandomFloorNumber(maxFloor, currentFloorNumber);
        this.currentFloorNumber = currentFloorNumber;
    }

    public Integer chooseNextRandomFloorNumber(Integer maxFloor, Integer currentFloorNumber) {

        while (true) {
            Integer randomFloor = (int) (Math.random() * maxFloor + 1);
            if (!randomFloor.equals(currentFloorNumber)) {
                return randomFloor;
            }
        }
    }

    public Passenger setDestinationFloor(Integer destinationFloor) {
        if (Objects.equals(destinationFloor, currentFloorNumber)) {
            throw new UnsupportedOperationException("Passenger cannon have destination to the same floor, where hi is located!");
        }
        this.destinationFloor = destinationFloor;
        return this;
    }

    public Passenger setCurrentFloorNumber(Integer currentFloorNumber) {
        this.currentFloorNumber = currentFloorNumber;
        return this;
    }

    public Integer getDestinationFloor() {
        return destinationFloor;
    }

    @Override
    public String toString() {
        return destinationFloor.toString();
    }
}
