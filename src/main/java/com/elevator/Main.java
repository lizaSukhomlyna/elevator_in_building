package com.elevator;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        int totalFloorNumber = (int) (Math.random() * (20 - 5 + 1)) + 5;

        List<Floor> floors = IntStream.range(1, totalFloorNumber + 1)
                .mapToObj(floorNumber ->
                        IntStream.range(1, (int) (Math.random() * 11) + 1)
                                .boxed()
                                .map(passengerNumber -> new Passenger(totalFloorNumber, floorNumber))
                                .collect(
                                        Collectors.collectingAndThen(
                                                Collectors.<Passenger>toList(),
                                                persons -> new Floor(floorNumber, persons, null)
                                        )
                                )
                ).collect(Collectors.toList());

        Building building = new Building(floors, new Elevator(totalFloorNumber));
        building.startWork();

    }

}
