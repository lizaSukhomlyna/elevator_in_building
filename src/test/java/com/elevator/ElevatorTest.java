package com.elevator;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ElevatorTest {
    @Test
    void noPassengersOnTheFloorAndLowFloor() {
        Elevator tested = new Elevator(3);
        tested.setCurrentFloorNumber(1);
        Elevator.Direction provided = tested.chooseDirection(Collections.emptyList()).getDirection();

        Elevator.Direction expected = Elevator.Direction.UP;

        assertThat(provided).isEqualTo(expected);
    }

    @Test
    void noPassengersOnTheFloorAndTopFloor() {
        Elevator tested = new Elevator(3);
        tested.setCurrentFloorNumber(3);
        Elevator.Direction provided = tested.chooseDirection(Collections.emptyList()).getDirection();

        Elevator.Direction expected = Elevator.Direction.DOWN;

        assertThat(provided).isEqualTo(expected);
    }

    @Test
    void hasPassengersWithLowerFloorDestinationFromTop() {
        Elevator tested = new Elevator(3);
        tested.setCurrentFloorNumber(3);
        Elevator.Direction provided = tested.chooseDirection(Collections.singletonList(new Passenger(3, 3))).getDirection();

        Elevator.Direction expected = Elevator.Direction.DOWN;

        assertThat(provided).isEqualTo(expected);
    }

    @Test
    void hasPassengersWithTopFloorDestinationFromLower() {
        Elevator tested = new Elevator(3);
        tested.setCurrentFloorNumber(1);
        Elevator.Direction provided = tested.chooseDirection(Collections.singletonList(new Passenger(3, 1))).getDirection();

        Elevator.Direction expected = Elevator.Direction.UP;

        assertThat(provided).isEqualTo(expected);
    }

    @Test
    void hasPassengersWithTopAndLowFloorDestinationFromMiddle() {
        Elevator tested = new Elevator(3);
        tested.setCurrentFloorNumber(2);
        List<Passenger> providedFloorPassengers = Arrays.asList(
                new Passenger(3, 2).setDestinationFloor(1),
                new Passenger(3, 2).setDestinationFloor(3)
        );
        Elevator.Direction provided = tested.chooseDirection(providedFloorPassengers).getDirection();

        Elevator.Direction defaultExpectedDirection = Elevator.Direction.DOWN;

        assertThat(provided).isEqualTo(defaultExpectedDirection);
    }

    @Test
    void hasTwoPassengersWithTopAndOneWithLowFloorDestinationFromMiddle() {
        Elevator tested = new Elevator(3);
        tested.setCurrentFloorNumber(2);
        List<Passenger> providedFloorPassengers = Arrays.asList(
                new Passenger(3, 2).setDestinationFloor(1),
                new Passenger(3, 2).setDestinationFloor(3),
                new Passenger(3, 2).setDestinationFloor(3)
        );
        Elevator.Direction provided = tested.chooseDirection(providedFloorPassengers).getDirection();

        Elevator.Direction expectedDirection = Elevator.Direction.UP;

        assertThat(provided).isEqualTo(expectedDirection);
    }

    @Test
    void hasTwoPassengersWithLowAndOneWithTopFloorDestinationFromMiddle() {
        Elevator tested = new Elevator(3);
        tested.setCurrentFloorNumber(2);
        List<Passenger> providedFloorPassengers = Arrays.asList(
                new Passenger(3, 2).setDestinationFloor(1),
                new Passenger(3, 2).setDestinationFloor(1),
                new Passenger(3, 2).setDestinationFloor(3)
        );
        Elevator.Direction provided = tested.chooseDirection(providedFloorPassengers).getDirection();

        Elevator.Direction expectedDirection = Elevator.Direction.DOWN;

        assertThat(provided).isEqualTo(expectedDirection);
    }
}